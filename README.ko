			  Texinfo 한글 패치

이 패키지는 ksc5601로 인코딩된 Texinfo 형식의 한글 문서를 작성할 수
있도록 Texinfo를 수정한 것입니다.


패치내용
********

* TeX 포맷터

이 부분이 Texinfo 한글패치의 중점 패치 대상입니다.  가장 할 일도 많고,
복잡한 문제이기 때문에..

현재 HLaTeX 0.99이상 (0.99, 0.991)의 완성형 글꼴에 맞춰서 만들어졌기
때문에 다른 한글 TeX 시스템, 혹은 다른 형식의 글꼴(0.98 글꼴, 0.95의
조합형 글꼴, 0.99이상의 uhc글꼴) 에서는 사용이 불가능합니다.

Texinfo 소스에서 texinfo.tex 매크로 파일 대신 한글 매크로로
texinfo-ko.tex을 사용합니다.  한글 문서에서 맨 첫머리에 `\input
texinfo.tex' 줄 대신에 `\input texinfo-ko.tex'을 쓰면 됩니다.  이것은
한글 문서를 구분하는 기준으로도 쓰일 수 있을 것입니다.

* Info 포맷터

makeinfo는 8 bit clean이므로 한글 처리에 문제가 없습니다.  단,
조사처리와 같은 localize할 수밖에 없는 부분에 대해서 명령어를
추가합니다.

texindex에 한글 색인을 정렬하도록 추가하였음.

* Info 브라우저

Info 브라우저의 수정은 없을 것입니다.  Emacs의 info모드는 입, 출력
모두 잘 동작합니다.  "info" 프로그램의 경우 locale이 제대로 맞춰진
경우 출력은 잘 되지만, 8 비트 코드의 입력은 현재 불가능합니다.

이 문제는 코드상으로 수정하기 어려운 문제이며, Texinfo 메인테이너가
고쳐야 할 버그이지 localize할 일은 아닙니다.


설치방법
========

Texinfo의 소스와 함께 포함되어 있습니다.  설치방법은 다른 모든 GNU
패키지와 동일합니다.  `INSTALL'화일을 참조하십시오.

그리고 texinfo.ko.tex, texinfo.ko-HLaTeX097.tex,
eps.tex, texinfo.tex 화일을 $TEXINPUTS 디렉토리에 복사하십시오.  이
화일은 직접 손으로 설치해야 합니다.


파일
====

README.ko -- 지금 읽고 있는 파일
TODO.ko -- 앞으로 개선할 점
NEWS.ko -- 달라진 점
texinfo-ko.tex -- TeX용 한글 매크로
texinfo-ko-HLaTeX.tex -- HLaTeX 글꼴을 사용하기 위한 매크로

버그, 제안
==========

버그가 발견될 경우 `cwryu@debian.org로 Email을 보내 주십시오.
버그레포트를 할 경우, 애러 메세지와 함께 입력 texinfo 파일을
첨부하거나, FTP나 웹을 통해 구할 수 있는 위치를 알려주십시오.

버그 외에 글꼴 모양이 맘에 안 든다거나, 이렇게 하면 좋겠다는 권고도
얼마든지 환영입니다.  코드에 대한 개선점도 환영합니다.  

어떤 종류든지 성공담, 실패담 혹은 사용사례 등을 알려주시면 저에게 큰
도움이 될 것입니다.


그외에
======

* 웹페이지

최신버전과 몇개의 한글 매뉴얼을 모아놓은 웹페이지가 있습니다.

	 http://master.debian-kr.org/~cwryu/hacks/texinfo/

* License

이 패키지는 Texinfo를 수정한 것이므로 GNU General Public License
version 2 에 의해 배포됩니다.

--

류창우 <cwryu@adam.kaist.ac.kr>

