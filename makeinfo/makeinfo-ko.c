/* makeinfo-ko.c -- Korean language utility functions for makeinfo
   Copyright (C) 1998 Changwoo Ryu

   This program is free software; you can redistribute it and'or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA */

/* Written by Changwoo Ryu <cwryu@eve.kaist.ac.kr>. */

#ifdef TEXINFO_KO
#include <stdlib.h>
#include "makeinfo-ko.h"

static int no_chongsung[] =
{
  0xb0a1, /* �� */ 0xb0b3, /* �� */ 0xb0bc, /* �� */ 0xb0c2, /* �� */
  0xb0c5, /* �� */ 0xb0d4, /* �� */ 0xb0dc, /* �� */ 0xb0e8, /* �� */
  0xb0ed, /* �� */ 0xb0fa, /* �� */ 0xb1a5, /* �� */ 0xb1ab, /* �� */
  0xb1b3, /* �� */ 0xb1b8, /* �� */ 0xb1c5, /* �� */ 0xb1cb, /* �� */
  0xb1cd, /* �� */ 0xb1d4, /* �� */ 0xb1d7, /* �� */ 0xb1e1, /* �� */
  0xb1e2, /* �� */ 0xb1ee, /* �� */ 0xb1fa, /* �� */ 0xb2a5, /* �� */
  0xb2a8, /* �� */ 0xb2b2, /* �� */ 0xb2b8, /* �� */ 0xb2be, /* �� */
  0xb2bf, /* �� */ 0xb2ca, /* �� */ 0xb2cf, /* �� */ 0xb2d2, /* �� */
  0xb2d8, /* �� */ 0xb2d9, /* �� */ 0xb2e3, /* �� */ 0xb2e7, /* �� */
  0xb2ee, /* �� */ 0xb2f3, /* �� */ 0xb2f4, /* �� */ 0xb3a2, /* �� */
  0xb3aa, /* �� */ 0xb3bb, /* �� */ 0xb3c4, /* �� */ 0xb3ca, /* �� */
  0xb3d7, /* �� */ 0xb3e0, /* �� */ 0xb3e9, /* �� */ 0xb3eb, /* �� */
  0xb3f6, /* �� */ 0xb3fa, /* �� */ 0xb4a2, /* �� */ 0xb4a9, /* �� */
  0xb4b2, /* �� */ 0xb4b4, /* �� */ 0xb4b5, /* �� */ 0xb4c0, /* �� */
  0xb4cc, /* �� */ 0xb4cf, /* �� */ 0xb4d9, /* �� */ 0xb4eb, /* �� */
  0xb4f4, /* �� */ 0xb4f5, /* �� */ 0xb5a5, /* �� */ 0xb5ae, /* �� */
  0xb5b3, /* �� */ 0xb5b5, /* �� */ 0xb5c2, /* �� */ 0xb5c7, /* �� */
  0xb5cd, /* �� */ 0xb5ce, /* �� */ 0xb5d6, /* �� */ 0xb5d8, /* �� */
  0xb5da, /* �� */ 0xb5e0, /* �� */ 0xb5e5, /* �� */ 0xb5ef, /* �� */
  0xb5f0, /* �� */ 0xb5fb, /* �� */ 0xb6a7, /* �� */ 0xb6b0, /* �� */
  0xb6bc, /* �� */ 0xb6c5, /* �� */ 0xb6c7, /* �� */ 0xb6cc, /* �� */
  0xb6ce, /* �� */ 0xb6cf, /* �� */ 0xb6d1, /* �� */ 0xb6d8, /* �� */
  0xb6d9, /* �� */ 0xb6df, /* �� */ 0xb6e7, /* �� */ 0xb6ec, /* �� */
  0xb6f3, /* �� */ 0xb7a1, /* �� */ 0xb7aa, /* �� */ 0xb7af, /* �� */
  0xb7b9, /* �� */ 0xb7c1, /* �� */ 0xb7ca, /* �� */ 0xb7ce, /* �� */
  0xb7d6, /* �� */ 0xb7da, /* �� */ 0xb7e1, /* �� */ 0xb7e7, /* �� */
  0xb7ef, /* �� */ 0xb7f2, /* �� */ 0xb7f9, /* �� */ 0xb8a3, /* �� */
  0xb8ae, /* �� */ 0xb8b6, /* �� */ 0xb8c5, /* �� */ 0xb8cf, /* �� */
  0xb8d3, /* �� */ 0xb8de, /* �� */ 0xb8e7, /* �� */ 0xb8ef, /* �� */
  0xb8f0, /* �� */ 0xb8fa, /* �� */ 0xb8fe, /* �� */ 0xb9a6, /* �� */
  0xb9ab, /* �� */ 0xb9b9, /* �� */ 0xb9be, /* �� */ 0xb9bf, /* �� */
  0xb9c2, /* �� */ 0xb9c7, /* �� */ 0xb9cc, /* �� */ 0xb9d9, /* �� */
  0xb9e8, /* �� */ 0xb9f2, /* �� */ 0xb9f6, /* �� */ 0xbaa3, /* �� */
  0xbaad, /* �� */ 0xbab6, /* �� */ 0xbab8, /* �� */ 0xbac1, /* �� */
  0xbac4, /* �� */ 0xbac6, /* �� */ 0xbacc, /* �� */ 0xbace, /* �� */
  0xbadb, /* �� */ 0xbade, /* �� */ 0xbadf, /* �� */ 0xbae4, /* �� */
  0xbaea, /* �� */ 0xbaf1, /* �� */ 0xbafc, /* �� */ 0xbba9, /* �� */
  0xbbb2, /* �� */ 0xbbb5, /* �� */ 0xbbbe, /* �� */ 0xbbc0, /* �� */
  0xbbc7, /* �� */ 0xbbcf, /* �� */ 0xbbd8, /* �� */ 0xbbda, /* �� */
  0xbbdf, /* �� */ 0xbbe7, /* �� */ 0xbbf5, /* �� */ 0xbbfe, /* �� */
  0xbca8, /* �� */ 0xbcad, /* �� */ 0xbcbc, /* �� */ 0xbcc5, /* �� */
  0xbcce, /* �� */ 0xbcd2, /* �� */ 0xbcdd, /* �� */ 0xbce2, /* �� */
  0xbce8, /* �� */ 0xbcee, /* �� */ 0xbcf6, /* �� */ 0xbda4, /* �� */
  0xbda6, /* �� */ 0xbdac, /* �� */ 0xbdb4, /* �� */ 0xbdba, /* �� */
  0xbdc3, /* �� */ 0xbdce, /* �� */ 0xbdd8, /* �� */ 0xbde1, /* �� */
  0xbdea, /* �� */ 0xbdee, /* �� */ 0xbdf7, /* �� */ 0xbdfb, /* �� */
  0xbdfd, /* �� */ 0xbea4, /* �� */ 0xbea5, /* �� */ 0xbeae, /* �� */
  0xbeaf, /* �� */ 0xbeb2, /* �� */ 0xbeba, /* �� */ 0xbebe, /* �� */
  0xbec6, /* �� */ 0xbed6, /* �� */ 0xbedf, /* �� */ 0xbeea, /* �� */
  0xbeee, /* �� */ 0xbfa1, /* �� */ 0xbfa9, /* �� */ 0xbfb9, /* �� */
  0xbfc0, /* �� */ 0xbfcd, /* �� */ 0xbfd6, /* �� */ 0xbfdc, /* �� */
  0xbfe4, /* �� */ 0xbfec, /* �� */ 0xbff6, /* �� */ 0xbffe, /* �� */
  0xc0a7, /* �� */ 0xc0af, /* �� */ 0xc0b8, /* �� */ 0xc0c7, /* �� */
  0xc0cc, /* �� */ 0xc0da, /* �� */ 0xc0e7, /* �� */ 0xc0f0, /* �� */
  0xc0f7, /* �� */ 0xc0fa, /* �� */ 0xc1a6, /* �� */ 0xc1ae, /* �� */
  0xc1b5, /* �� */ 0xc1b6, /* �� */ 0xc1c2, /* �� */ 0xc1c8, /* �� */
  0xc1cb, /* �� */ 0xc1d2, /* �� */ 0xc1d6, /* �� */ 0xc1e0, /* �� */
  0xc1e2, /* �� */ 0xc1e3, /* �� */ 0xc1ea, /* �� */ 0xc1ee, /* �� */
  0xc1f6, /* �� */ 0xc2a5, /* ¥ */ 0xc2b0, /* ° */ 0xc2b9, /* ¹ */
  0xc2bc, /* ¼ */ 0xc2c5, /* �� */ 0xc2c9, /* �� */ 0xc2d2, /* �� */
  0xc2d6, /* �� */ 0xc2d8, /* �� */ 0xc2de, /* �� */ 0xc2e5, /* �� */
  0xc2e8, /* �� */ 0xc2e9, /* �� */ 0xc2ea, /* �� */ 0xc2ee, /* �� */
  0xc2f7, /* �� */ 0xc3a4, /* ä */ 0xc3ad, /* í */ 0xc3b3, /* ó */
  0xc3bc, /* ü */ 0xc3c4, /* �� */ 0xc3c7, /* �� */ 0xc3ca, /* �� */
  0xc3d2, /* �� */ 0xc3d6, /* �� */ 0xc3dd, /* �� */ 0xc3df, /* �� */
  0xc3e7, /* �� */ 0xc3eb, /* �� */ 0xc3f2, /* �� */ 0xc3f7, /* �� */
  0xc4a1, /* ġ */ 0xc4ab, /* ī */ 0xc4ab, /* ī */ 0xc4b3, /* ĳ */
  0xc4bc, /* ļ */ 0xc4bf, /* Ŀ */ 0xc4c9, /* �� */ 0xc4d1, /* �� */
  0xc4d9, /* �� */ 0xc4da, /* �� */ 0xc4e2, /* �� */ 0xc4e8, /* �� */
  0xc4ea, /* �� */ 0xc4ec, /* �� */ 0xc4ed, /* �� */ 0xc4f5, /* �� */
  0xc4f9, /* �� */ 0xc4fb, /* �� */ 0xc5a5, /* ť */ 0xc5a9, /* ũ */
  0xc5b0, /* Ű */ 0xc5b8, /* Ÿ */ 0xc5c2, /* �� */ 0xc5cb, /* �� */
  0xc5cd, /* �� */ 0xc5d7, /* �� */ 0xc5df, /* �� */ 0xc5e2, /* �� */
  0xc5e4, /* �� */ 0xc5ed, /* �� */ 0xc5ef, /* �� */ 0xc5f0, /* �� */
  0xc5f4, /* �� */ 0xc5f5, /* �� */ 0xc5fd, /* �� */ 0xc6a2, /* Ƣ */
  0xc6a9, /* Ʃ */ 0xc6ae, /* Ʈ */ 0xc6b7, /* Ʒ */ 0xc6bc, /* Ƽ */
  0xc6c4, /* �� */ 0xc6d0, /* �� */ 0xc6d9, /* �� */ 0xc6db, /* �� */
  0xc6e4, /* �� */ 0xc6ec, /* �� */ 0xc6f3, /* �� */ 0xc6f7, /* �� */
  0xc7a1, /* ǡ */ 0xc7a3, /* ǣ */ 0xc7a5, /* ǥ */ 0xc7aa, /* Ǫ */
  0xc7b4, /* Ǵ */ 0xc7b6, /* Ƕ */ 0xc7bb, /* ǻ */ 0xc7c1, /* �� */
  0xc7c7, /* �� */ 0xc7cf, /* �� */ 0xc7d8, /* �� */ 0xc7e1, /* �� */
  0xc7e3, /* �� */ 0xc7ec, /* �� */ 0xc7f4, /* �� */ 0xc7fd, /* �� */
  0xc8a3, /* ȣ */ 0xc8ad, /* ȭ */ 0xc8b3, /* ȳ */ 0xc8b8, /* ȸ */
  0xc8bf, /* ȿ */ 0xc8c4, /* �� */ 0xc8cc, /* �� */ 0xc8d1, /* �� */
  0xc8d6, /* �� */ 0xc8de, /* �� */ 0xc8e5, /* �� */ 0xc8f1, /* �� */
  0xc8f7, /* �� */
};

/* NO_CHONGSUNG_NUMBER == 341 */
#define NO_CHONGSUNG_NUMBER (sizeof(no_chongsung)/sizeof(no_chongsung[1]))

static int
compare_int (a, b)
     const void *a, *b;
{
  return (*((int *)a)-*((int *)b));
}

/* Returns nonzero only if "code" have chongsung(last sound),
   "code" is one KSC-5601 character, (<first byte> * 256 + <second byte>).  */
int
is_there_chongsung (code)
     int code;
{
  return !(bsearch(&code, no_chongsung,
                    NO_CHONGSUNG_NUMBER, sizeof(int),
                    compare_int));
}

#endif /* TEXINFO_KO */
